section .data

; org 0x00751528
global _DAT_00751528
_DAT_00751528: dd 0x3f000000 ; 0.5F

; org 0x0075ad9c
global _s_Episode_V_0075ad9c
_s_Episode_V_0075ad9c: db "Episode_V.dat", 0

; org 0x0075adac
global _s_Episode_IV_0075adac
_s_Episode_IV_0075adac: db "Episode_IV.dat", 0

; org 0x0075adbc
global _s_Episode_III_0075adbc
_s_Episode_III_0075adbc: db "Episode_III.dat", 0

; org 0x0075adcc
global _s_Episode_II_0075adcc
_s_Episode_II_0075adcc: db "Episode_II.dat", 0

; org 0x0075addc
global _s_Episode_I_0075addc
_s_Episode_I_0075addc: db "Episode_I.dat", 0

; org 0x007facf0
global _DAT_007facf0
_DAT_007facf0: dd 0x0

; org 0x007ff950
global _DAT_007ff950
_DAT_007ff950: dd 0x1

; org 0x00802364
global _DAT_00802364
_DAT_00802364: dd 0x6

; org 0x00802c54
global _PTR_DAT_00802c54
_PTR_DAT_00802c54: dd _DAT_0093d878

; org 0x0082763c
global _DAT_0082763c
_DAT_0082763c: dd 0

; org 0x00827664
global _DAT_00827664
_DAT_00827664: dd 0

section .bss

; org 0x008668f0
global _DAT_008668f0
_DAT_008668f0: resb 1

; org 0x0087b560
global _DAT_0087b560
_DAT_0087b560: resb 4

; org 0x0087b564
global _DAT_0087b564
_DAT_0087b564: resb 4

; org 0x0087b568
global _DAT_0087b568
_DAT_0087b568: resb 4

; org 0x0087b56c
global _DAT_0087b56c
_DAT_0087b56c: resb 4

; org 0x0087b570
global _DAT_0087b570
_DAT_0087b570: resb 4

; org 0x0087b574
global _DAT_0087b574
_DAT_0087b574: resb 4

; org 0x0087b578
global _DAT_0087b578
_DAT_0087b578: resb 4

; org 0x00924884
global _DAT_00924884
_DAT_00924884: resb 4

; org 0x0093b358
global _DAT_0093b358
_DAT_0093b358: resb 4

; org 0x0093d878
global _DAT_0093d878
_DAT_0093d878: resb 4

; org 0x02975ab0
global _DAT_02975ab0
_DAT_02975ab0: resb 4

; org 0x0297dbe0
global _DAT_0297dbe0
_DAT_0297dbe0: resb 4
